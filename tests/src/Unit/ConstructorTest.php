<?php

namespace Drupal\Tests\commerce_product_reservation\Unit;

use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_product_reservation\CartProvider;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test that we can construct what we override.
 *
 * @group commerce_product_reservation
 */
class ConstructorTest extends UnitTestCase {

  /**
   * Test that we can construct the thing, so it does not break on updates.
   */
  public function testConstructor() {
    $mock_etm = $this->createMock(EntityTypeManagerInterface::class);
    $mock_current_store = $this->createMock(CurrentStoreInterface::class);
    $mock_account = $this->createMock(AccountInterface::class);
    $mock_cart = $this->createMock(CartSessionInterface::class);
    // Now try to construct our class, and then we just assert something, to
    // make sure the test runs.
    new CartProvider($mock_etm, $mock_current_store, $mock_account, $mock_cart);
    $this->assertEquals(TRUE, TRUE);
  }

}
