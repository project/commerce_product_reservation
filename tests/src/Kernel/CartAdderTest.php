<?php

namespace Drupal\Tests\commerce_product_reservation\Kernel;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_store\Entity\Store;
use Drupal\Tests\commerce_cart\Kernel\CartKernelTestBase;

/**
 * Test the cart adder.
 *
 * @group commerce_product_reservation
 */
class CartAdderTest extends CartKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_product_reservation',
    'commerce_product_reservation_simple',
    'commerce_checkout',
    'telephone',
  ];

  /**
   * {@inheritdoc}
   */
  public function setup() : void {
    parent::setup();
    $this->installConfig(['commerce_product_reservation']);
  }

  /**
   * Test adding.
   */
  public function testAdder() {
    /** @var \Drupal\commerce_product_reservation\CartAdder $adder */
    $adder = $this->container->get('commerce_product_reservation.cart_adder');
    $product_variation = ProductVariation::create([
      'sku' => '123',
      'type' => 'default',
    ]);
    $product_variation->save();
    $store = Store::create([
      'type' => 'online',
      'is_default' => TRUE,
    ]);
    $store->save();
    $product = Product::create([
      'type' => 'default',
      'title' => 'Test product',
      'stores' => [$store],
      'variations' => [$product_variation],
    ]);
    $product->save();
    /** @var \Drupal\commerce_product_reservation\SelectedStoreManager $selected_store */
    $selected_store = $this->container->get('commerce_product_reservation.selected_store');
    $selected_store->setSelectedStore('simple_store', 'simple_store');
    $order_item = $adder->addEntity($product_variation, 1);
    self::assertInstanceOf(OrderItemInterface::class, $order_item);
  }

}
