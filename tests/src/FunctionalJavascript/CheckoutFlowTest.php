<?php

namespace Drupal\Tests\commerce_product_reservation\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\commerce\Traits\DeprecationSuppressionTrait;

/**
 * Javascript tests for this.
 *
 * @group commerce_product_reservation
 */
class CheckoutFlowTest extends WebDriverTestBase {

  use DeprecationSuppressionTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $failOnJavascriptConsoleErrors = FALSE;

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_product_reservation',
    'commerce_tax',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    $this->setErrorHandler();
    parent::setUp();
    $this->adminUser = $this->createUser(['administer commerce_checkout_flow']);
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() : void {
    parent::tearDown();
    $this->restoreErrorHandler();
  }

  /**
   * {@inheritdoc}
   */
  protected function setErrorHandler() {
    $previous_error_handler = set_error_handler(function ($severity, $message, $file, $line, $context = NULL) use (&$previous_error_handler) {

      $skipped_deprecations = [
        'Javascript Deprecation: jQuery.once() is deprecated in Drupal 9.3.0 and will be removed in Drupal 10.0.0. Use the core/once library instead. See https://www.drupal.org/node/3158256',
      ];

      if (!in_array($message, $skipped_deprecations, TRUE)) {
        return $previous_error_handler($severity, $message, $file, $line, $context);
      }
    }, E_USER_DEPRECATED);
  }

  /**
   * Test the fact that we have a checkout flow for reservations.
   */
  public function testCheckoutFlowExists() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/commerce/config/checkout-flows/manage/reservation');
    $this->assertSession()->elementExists('css', '#commerce-checkout-flow-edit-form');
  }

}
