<?php

namespace Drupal\commerce_product_reservation\Exception;

/**
 * Exception class for no stock.
 */
class NoStockResultException extends \Exception {}
