<?php

namespace Drupal\commerce_product_reservation\Exception;

/**
 * Class to indicate an availability exception.
 */
class AvailabilityException extends \Exception {}
