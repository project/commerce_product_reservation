<?php

namespace Drupal\commerce_product_reservation;

use Drupal\commerce\AvailabilityManagerInterface;
use Drupal\commerce\Context;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_reservation\Exception\AvailabilityException;
use Drupal\commerce_product_reservation\Exception\NoStockResultException;
use Drupal\commerce_product_reservation\Exception\OutOfStockException;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_store\SelectStoreTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Our own cart adder.
 */
class CartAdder {
  use SelectStoreTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Availability manager.
   *
   * @var \Drupal\commerce\AvailabilityManagerInterface
   */
  protected $availabilityManager;

  /**
   * Current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * Cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Selected store.
   *
   * @var \Drupal\commerce_product_reservation\SelectedStoreManager
   */
  protected $selectedStore;

  /**
   * Store plugin manager.
   *
   * @var \Drupal\commerce_product_reservation\ReservationStorePluginManager
   */
  protected $storePluginManager;

  /**
   * Constructs a cartadder object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CartManagerInterface $cartManager, AvailabilityManagerInterface $availabilityManager, CurrentStoreInterface $current_store, CartProviderInterface $cart_provider, AccountProxyInterface $current_user, SelectedStoreManager $selectedStore, ReservationStorePluginManager $storePluginManager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cartManager = $cartManager;
    $this->availabilityManager = $availabilityManager;
    $this->currentStore = $current_store;
    $this->cartProvider = $cart_provider;
    $this->currentUser = $current_user;
    $this->selectedStore = $selectedStore;
    $this->storePluginManager = $storePluginManager;
  }

  /**
   * Add by SKU.
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\commerce_product_reservation\Exception\NoStockResultException
   * @throws \Drupal\commerce_product_reservation\Exception\OutOfStockException
   * @throws \Drupal\commerce_product_reservation\Exception\AvailabilityException
   */
  public function addBySku($sku, $quantity = 1) {
    /** @var \Drupal\commerce_product\ProductVariationStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('commerce_product_variation');
    if (!$variation = $storage->loadBySku($sku)) {
      throw new \Exception('No product found for SKU ' . $sku);
    }
    return $this->addEntity($variation, $quantity);
  }

  /**
   * Helper.
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\commerce_product_reservation\Exception\NoStockResultException
   * @throws \Drupal\commerce_product_reservation\Exception\OutOfStockException
   * @throws \Drupal\commerce_product_reservation\Exception\AvailabilityException
   */
  public function addEntity(ProductVariationInterface $variation, $quantity) {
    $current_store = $this->selectedStore->getSelectedStore();
    if (!$current_store) {
      throw new \InvalidArgumentException('Trying to add to a reservation cart without having a selected store');
    }
    // Load the plugin. This can trigger an exception, which is fine if we can
    // not find it.
    /** @var \Drupal\commerce_product_reservation\ReservationStoreInterface $plugin */
    $plugin = $this->storePluginManager->createInstance($current_store->getProvider());
    /** @var \Drupal\commerce_product_reservation\StockResult[] $stock_results */
    $stock_results = $plugin->getStockByStoresAndProducts([$current_store], [$variation->getSku()]);
    if (empty($stock_results)) {
      throw new NoStockResultException('No stock result found before adding it to cart');
    }
    $stock_result = reset($stock_results);
    if ($stock_result->getStock() < $quantity) {
      $e = new OutOfStockException('Quantity was bigger than the stock value');
      $e->setMaxQuantity($stock_result->getStock());
      throw $e;
    }
    $store = $this->selectStore($variation);
    // Now see if it is at all possible to add it. Seems like this should be
    // unnecessary, so we have this issue opened for it:
    // https://www.drupal.org/project/commerce/issues/3023417.
    // If that ever gets fixed, this part should be removed.
    $context = new Context($this->currentUser, $store);
    if (!$this->availabilityManager->check($variation, $quantity, $context)) {
      throw new AvailabilityException('The item could was not allowed to add to the cart');
    }
    $order_type = $this->getOrderType();
    $cart = $this->cartProvider->getCart($order_type, $store, $this->currentUser);
    if (!$cart) {
      $cart = $this->cartProvider->createCart($order_type, $store, $this->currentUser);
    }
    return $this->cartManager->addEntity($cart, $variation, $quantity);
  }

  /**
   * Get the order type to use.
   */
  protected function getOrderType() {
    return ReservationManagerInterface::ORDER_TYPE;
  }

}
