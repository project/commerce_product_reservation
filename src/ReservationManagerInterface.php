<?php

namespace Drupal\commerce_product_reservation;

/**
 * Interface for the manager.
 */
interface ReservationManagerInterface {
  const ORDER_TYPE = 'reservation_order';

}
